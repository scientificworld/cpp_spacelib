/*
* spacelib_filename_h , version 1.0
* This file is included in spacelib.
* It is under the WTFPL licence.
*/

#include <string>
// #include "strutil.h"

#ifndef _SPACELIB_FILENAME_H_
#define _SPACELIB_FILENAME_H_

namespace space {
	std::string sffn(std::string full) { int fd=full.find('.'); if(fd!=std::string::npos) return full.substr(0,fd); else return NULL; }
	std::string slfn(std::string full) { int fd=full.rfind('.'); if(fd!=std::string::npos) return full.substr(fd); else return NULL; }
	std::string lffn(std::string full) { int fd=full.rfind('.'); if(fd!=std::string::npos) return full.substr(0,fd); else return NULL; }
	std::string llfn(std::string full) { int fd=full.find('.'); if(fd!=std::string::npos) return full.substr(fd); else return NULL; }
}

#endif
