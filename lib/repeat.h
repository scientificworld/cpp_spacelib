/*
* spacelib_repeat_h , version 1.0
* This file is included in spacelib.
* It is under the WTFPL licence.
*/

#ifndef _SLIB_REPEAT_H_
#define _SLIB_REPEAT_H_

#define repeat(__num) for(int __rep=0;__rep<__num;__rep++)

#endif
