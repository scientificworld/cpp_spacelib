#include "tm_bool.h"
namespace space {
	tm_bool rps_wl(tm_bool a,tm_bool b){
		if(a==1&&b==2){
			return COR;
		}
		if(a==1&&b==3){
			return ERR;
		}
		if(a==2&&b==1){
			return ERR;
		}
		if(a==2&&b==3){
			return COR;
		}
		if(a==3&&b==1){
			return ERR;
		}
		if(a==3&&b==2){
			return COR;
		}
		if(a==b){
			return NA;
		}
	}
}
