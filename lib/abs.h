/*
* spacelib_abs_h , version 1.0
* This file is included in spacelib.
* It is under the WTFPL licence.
*/

#ifndef _SLIB_ABS_H_
#define _SLIB_ABS_H_

namespace space {
	template <class T> T _abs(T x) {
		if(x>=0) return x;
		else return -x;
	}
}

#endif
