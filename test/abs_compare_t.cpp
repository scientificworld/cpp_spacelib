#include <iostream>
#include "../lib/abs.h"
#include "../lib/compare.h"
using namespace space;

int main(){
	int a,b;
	std::cin>>a>>b;
	std::cout<<_abs(a)<<' '<<_abs(b)<<' '<<_max(a,b)<<' '<<_min(a,b);
	return 0;
}
