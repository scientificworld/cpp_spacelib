/*
* spacelib_compare_h , version 1.0
* This file is included in spacelib.
* It is under the WTFPL licence.
*/

#ifndef _SLIB_COMPARE_H_
#define _SLIB_COMPARE_H_

namespace space {
	template <class T> T _max(T x,T y) {
		if(x>=y) return x;
		else return y;
	}
	template <class T> T _min(T x,T y) {
		if(x<=y) return x;
		else return y;
	}
}

#endif
