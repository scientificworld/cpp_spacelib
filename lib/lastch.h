/*
* spacelib_lastch_h , version 1.0
* This file is included in spacelib.
* It is under the WTFPL licence.
*/

#include <string>

#ifndef _SPACELIB_LASTCH_H_
#define _SPACELIB_LASTCH_H_

namespace space {
	char lastch(std::string str) return str[str.size()-1];
}

#endif
